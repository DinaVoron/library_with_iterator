#pragma once
#define TreeHeader
#include <iostream>

using namespace std;

struct card_node {
    card_node *next;
    Book *data;
};


template <typename T>
struct Node
{
private:
    int height = 0;
    T author;
    Node* left;
    Node* right;
    Node* parent;
    card_node* list = nullptr;
        template<class Iter>
        class TreeIterator
        {
            friend class Node;
        public:
            typedef Iter iterator_type;
            typedef input_iterator_tag iterator_category;
            typedef iterator_type value_type;
            typedef ptrdiff_t difference_type;
            typedef iterator_type& reference;
            typedef iterator_type* pointer;
            iterator_type* value;
        private:
            TreeIterator(Iter* p) : value(p) {}
        public:
            TreeIterator(const TreeIterator &p) : value(p.value) {}

            bool operator !=(TreeIterator const& other) const
            {
                return value != other.value;
            }

            bool operator ==(TreeIterator const& other) const
            {
                return value == other.value;
            }

            typename TreeIterator::reference operator*() const
            {
                return *value;
            }

            TreeIterator& operator++() 
            {
                if (value->right != nullptr)
                {
                    value = value->right;
                    while (value->left != nullptr) {
                        value = value->left;
                    }
                }
                else {
                    Node* temp = value->parent;
                    while (temp->right == value) {
                        value = temp;
                        temp = value->parent;
                    }
                    value = temp;
                }
                return *this;
            }
        };

public:
    Node() {};
    Node(T auth) : author(auth), right(nullptr),left(nullptr),parent(nullptr) {};
    void set_left_node(Node* p);
    void set_right_node(Node* p);
    int heightt();
    int bfactor();
    void fixheight();
    Node* rotateright();
    Node* rotateleft();
    Node* balance();
    Node* findmin_right();
    Node* removemin();
    Node* delete_tree();
    Book* search(string author, string name);
    T get_author();
    Node* get_left();
    card_node* get_list();
    void set_list(card_node* p);
    Node* get_right();
    void set_parent(Node* p);
    Node* remove(card_struct node_info);
    Node(const Node& node) : author(node.author), left(node.left), right(node.right), parent(node.parent) {}
    typedef TreeIterator<Node> iterator;
    typedef TreeIterator<const Node> const_iterator;
    iterator begin();
    iterator end();
    const_iterator cbegin() const;
    const_iterator cend() const;
    friend ostream& operator <<(ostream& os, const Node& n)
    {
        os << n.author;
        return os;
    }
};


template <typename T>
class Tree //= 
{
public:
    Node<T>* root;
    Tree();
    Tree(Tree& tree) {};
    Tree(Tree&& tree) : root(move(tree.root)) {};
    Node<T>* insert(card_struct node_info, Node<T>* p);
    void print_tree();
    void read_file(ifstream& input_file);
    void return_book(Reader &reader, card_struct book);
};

string Node<string>::get_author()
{
    return author;
}

Node<string>* Node<string>::get_left()
{
    return left;
}


void Node<string>::set_left_node(Node<string>* p) {
    this->left = p;
}

void Node<string>::set_right_node(Node<string>* p) {
    this->right = p;
}

void Node<string>::set_list(card_node* p)
{
    this->list = p;
}

void Node<string>::set_parent(Node* p)
{
    this->parent = p;
}

Node<string>* Node<string>::get_right()
{
    return right;
}

card_node* Node<string>:: get_list()
{
    return list;
}

Tree<string>::Tree()
{
    root = nullptr;
}

Book* Node<string>::search(string author, string name)
{
    if (this == nullptr)
    {
        return nullptr;
    }
    else
    {
        if (author > this->author)
        {
            return this->right->search(author, name);
        }
        else
        {
            if (author < this->author)
            {
                return this->left->search(author, name);
            }
            else
            {
                card_node* q = this->list;
                while (q != nullptr)
                {
                    if (q->data->get_name() == name)
                    {
                        return q->data;
                    }
                    q = q->next;
                }
                return nullptr;
            }
        }
    }
}

void Tree<string>::return_book(Reader &reader, card_struct book)
{
    Book* q = this->root->search(book.author, book.name);
    if (q == nullptr)
    {
        cout << "This book doesn't exist";
    }
    else
    {
        if (reader.isPhilologist())
        {
            for (int i = 0; i < 15; i++)
            {
                if (book.number == reader.get_card_num(i))
                {
                    (this->root->search(book.author, book.name))->set_state(true);
                }
            }
            reader.set_reader_last(last(reader) + 1);
            if (last(reader) == 15)
            {
                reader.new_card();
            }
            reader.set_reader_state(true);
            cout << "Returned succesfully" << endl;
        }
        else
        {
            if (reader.get_last_card().number == book.number)
            {
                q->set_state(true);
                reader.set_reader_last(last(reader) + 1);
                if (last(reader) == 15)
                {
                    reader.new_card();
                }
                reader.set_reader_state(true);
                cout << "Returned succesfully" << endl;
            }
            else
            {
                cout << "Not right book" << endl;
            }
        }
    }
}


Node<string>::iterator Node<string>::begin()
{
    Node* node = this;
    while (node->left != nullptr) {
        node = node->left;
    }
    return iterator(node);
}

Node<string>::iterator Node<string>::end()
{
    Node* node = this;
    while (node->right != nullptr) {
        node = node->right;
    }
    return iterator(node);
}

Node<string>::const_iterator Node<string>::cbegin() const
{
    const Node* node = this;
    while (node->left != nullptr) {
        node = node->left;
    }
    return const_iterator(node);
}


Node<string>::const_iterator Node<string>::cend() const
{
    return nullptr;
}

int Node<string>::heightt()
{
    if (this != nullptr)
    {
        return height;
    }
    else
    {
        return 0;
    }
}

int Node<string>::bfactor() //��������� ������ ��������� ���� 
{
    if (this != nullptr)
    {
        return this->right->heightt() - this->left->heightt();
    }
}

void Node<string>::fixheight() //��������������� �������� ���� height ��������� ����
{
    int hl = this->left->heightt();
    int hr = this->right->heightt();
    if (hl > hr)
    {
        height = hl + 1;
    }
    else
    {
        height = hr + 1;
    }
}

Node<string>* Node<string>::rotateright() //������� �������
{
    Node* q2 = this->left;
    this->left = q2->right;
    if (q2->right != nullptr) {
        q2->right->parent = this;
    }
    q2->right = this;
    if (this != nullptr) {
        this->parent = q2;
    }
    this->fixheight();
    q2->fixheight();
    return q2;
}

Node<string>* Node<string>::rotateleft() //������� ������
{
    Node* p2 = this->right;
    this->right = p2->left;
    if (p2->left != nullptr) {
        p2->left->parent = this;
    }
    p2->left = this;
    if (this != nullptr) {
        this->parent = p2;
    }
    this->fixheight();
    p2->fixheight();
    return p2;
}

Node<string>* Node<string>::balance() //����������� ������ � ������ p 
{
    this->fixheight();
    if (this->bfactor() == 2) //�����
    {
        if (this->right->bfactor() < 0)
        {
            Node* q = this->right->rotateright();
            this->right = q;
            q->parent = this;
        }
        return this->rotateleft();
    }
    if (this->bfactor() == -2) //������
    {
        if (this->left->bfactor() > 0)
        {
            Node* q = this->left->rotateleft();
            this->left = q;
            q->parent = this;
        }
        return this->rotateright();
    }
    return this;
}

Node<string>* Tree<string>::insert(card_struct node_info, Node<string>* p) //Node* p = Tree.node ������� �� ���������
{
    if (p == nullptr) //���� ������ ������
    {
        Node<string>* NewNode = new Node<string>(node_info.author);
        card_node* m = new card_node;
        Book* book = new Book(node_info);
        m->data = book;
        m->next = nullptr;
        NewNode->set_list(m);
        return NewNode;
    }
    if (node_info.author < p->get_author())
    {
        Node<string>* q = insert(node_info, p->get_left());
        p->set_left_node(q); //��� ��� ��� ��������
        q->set_parent(p);
    }
    else
    {
        if (node_info.author > p->get_author())
        {
            Node<string>* q = insert(node_info, p->get_right());
            p->set_right_node(q);
            q->set_parent(p);
        }
        else
        {
            //� ����� �������� ������ � �������
            card_node* m = new card_node;
            card_node* q = p->get_list();
            if (q == nullptr)
            {
                card_node* m = new card_node;
                Book* book = new Book(node_info); //����� ���� - �����
                m->data = book;
                m->next = nullptr;
                p->set_list(m);
            }
            else
            {
                while (q->next != nullptr)
                {
                    q = q->next;
                }
                card_node* m = new card_node;
                Book* book = new Book(node_info);
                m->data = book;
                m->next = nullptr;
                q->next = m;
            }
        }
    }
    return p->balance();
}

Node<string>* Node<string>::findmin_right() //���� ����������� ������
{
    if (this->left != nullptr)
    {
        return(this->left->findmin_right());
    }
    else
    {
        return this;
    }
}

Node<string>* Node<string>::removemin()
{
    if (this->left == nullptr)
        return this->right;
    this->left = this->left->removemin();
    return this->balance();
}

Node<string>* Node<string>::remove(card_struct node_info) //��������
{
    if (!this) return nullptr;
    if (node_info.author < this->author) {
        Node* q = this->left->remove(node_info);
        this->left = q;
        q->parent = this->left;
    }
    else {
        if (node_info.author > this->author) {
            Node* q = this->right->remove(node_info);
            this->right = q;
            q->parent = this->right;
        }
        else {
            Node* q = this->left;
            Node* r = this->right;
            delete this;
            if (r == nullptr)
            {
                return q;
            }
            Node* min = r->findmin_right();
            Node* q1;
            q1 = r->removemin();
            min->right = q1;
            q1->parent = min->right;
            min->left = q;
            q->parent = min->left;
            return min->balance();
        }
    }
    return this->balance();
}

void Tree<string>::print_tree() //����������� ������ c ������ p � h ���������
{
    auto iter = this->root->begin();
    while (iter != this->root->end())
    {
        cout << *iter << endl;
        ++iter;
    }
    cout << *iter << " " << endl;
}



void Tree<string>::read_file(ifstream& input_file) {
    string temp;
    string num = "";
    while (!(input_file.eof())) {
        card_struct node_info;
        getline(input_file, temp);
        auto it = temp.begin();
        auto it2 = temp.end();
        while (*it != ',') //��������� ����� �����
        {
            num = num + *it;
            it = it + 1;
        }
        node_info.number = stoi(num);
        it += 2;
        while (*it != ',') //��������� ������
        {
            node_info.author = node_info.author + *it;
            it = it + 1;
        }
        it += 2;
        while (*it != ',') //��������� ��������
        {
            node_info.name = node_info.name + *it;
            it = it + 1;
        }
        it += 2;
        num = "";
        while (it != it2) //��������� ����� ������
        {
            num = num + *it;
            it = it + 1;
        }
        node_info.department_n = stoi(num);
        node_info.refund = true;
        this->root = this->insert(node_info, this->root);
        this->root->set_parent(nullptr);
    }
}

Node<string>* Node<string>::delete_tree()
{
    if (this != nullptr)
    {
        this->right->delete_tree();
        this->left->delete_tree();
        while (this->list != nullptr)
        {
            card_node* q = this->list;
            this->list = this->list->next;
            delete(q);
        }
        delete(this);
    }
    return nullptr;
}