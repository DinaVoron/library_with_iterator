﻿#include <iostream> 
#include <string>
#include <fstream>
#include "Header.h"
#include "TreeHeader.h"
#include "BuilderHeader.h"
using namespace std;


int main()
{
    ifstream F1;
    F1.open("books.txt");
    Tree<string> *Bookshelf = new Tree<string>();

    // список книг в фонде есть в файле в папке
    Bookshelf->read_file(F1);
    // выведем этот список
    Bookshelf->print_tree();

    // создадим нового уже не просто читателя человека Диану
    Reader Diana = Reader();
    
    // пусть Диана хочет взять книгу
    Diana.take_book(move(*Bookshelf->root->search("Jodi Picoult","Nineteen Minutes")));

    // пусть теперь Диана хочет взять еще одну книгу, но так нельзя по правилам библиотеки
    // если же человек - филолог (данная характеристика случаным образом выдается человеку)
    // то он может брать в библиотеке сколько угодно книг
    Diana.take_book(move(*Bookshelf->root->search("Fyodor Dostoevsky", "Crime and Punishment")));

    // Диана все поняла и теперь возвращает первую книгу
    Bookshelf->return_book(Diana, Diana.get_last_card());

    // тут в библиотеке появляется новый читатель
    Reader Rina = Reader();
    // Рина берет эту книгу
    Rina.take_book(move(*Bookshelf->root->search("Fyodor Dostoevsky", "Crime and Punishment")));

    // Диана уже собирается взять эту книгу, но она занята
    Diana.take_book(move(*Bookshelf->root->search("Fyodor Dostoevsky", "Crime and Punishment")));

    // Диана берет другую книгу
    Diana.take_book(move(*Bookshelf->root->search("James Joyce", "Ulysses")));

    // а потом она пытается вернуть Ринину книгу в библиотеку
    Bookshelf->return_book(Diana, Rina.get_last_card());

    // Диана не может вспомнить, какую же книгу она брала, поэтому смотрит в карточку читателя
    cout << Diana.get_last_card().name << " " << Diana.get_last_card().author << endl;

    // узнав, что нужно вернуть, она приносит взятую книгу
    Bookshelf->return_book(Diana, Diana.get_last_card());

    // представим, что Диана брала и сдавала 13 раз одну и ту же книгу
    for (int i = 0; i < 14; i++)
    {
        if (i == 5)
        {
            cout << "yes" << endl;
        }
        Diana.take_book(move(*Bookshelf->root->search("Leonid Andreev", "Iuda Iskariot")));
        Bookshelf->return_book(Diana, Diana.get_last_card());
    }

    // можно заметить, что на Диану завели новую книгу читателя, так как старая заполнилась
    cout << (last(Diana) + 1) << endl;

    // Рина возвращает книгу
    Bookshelf->return_book(Rina, Rina.get_last_card());
    // И берет новую
    Rina.take_book(move(*Bookshelf->root->search("Joe Abercrombie", "The Blade Itself")));


    // предположим, что с книгой что-то случилось
    // по правилам библиотеки человек должен купить такую же
    card_struct new_book = card_struct(Rina.get_last_card());
    // как мы видим, Диана спокойно возвращает книгу, сообщения о том, что книга не та, не последовало
    Bookshelf->return_book(Rina, new_book);

    // проверим, филолог ли Рина
    cout << "filologyst? " << Rina.isPhilologist() << endl;


    //Рина тоже не филолог, но мы сделаем Татьяну Викторовну, она-то уж точно филолог
    Reader Tatyana = Reader();
    cout << "filologyst? " << Tatyana.isPhilologist() << endl;
    //теперь проверим, что она может взять несколько книг
    Tatyana.take_book(move(*Bookshelf->root->search("Leonid Andreev", "Iuda Iskariot")));
    Tatyana.take_book(move(*Bookshelf->root->search("Yevgeny Zamyatin", "We")));
    //как мы видим, никакого сообщения о необходимости возврата книги не было выведено

    //посмотрим, являются ли читатели одновременно людьми
    Diana.Live();
    Rina.Laugh();
    Tatyana.Love();

    //в нашей библиотеке появились электронные книги!
    eBook b0 = eBook(1090, "Lomachinsky", "Medical Examiner's Stories", 84);
    Diana.take_book(move(b0));

    //сделаем в нашей библиотеке какую-нибудь встречу
    EventBuilder *our_event_builder = new ClassicEventBuilder();
    LibraryDirector *director = new LibraryDirector(move(*our_event_builder));
    Event *our_event = director->MakeEvent();
    if (our_event != nullptr)
    {
        cout << "We've made the new event";
    }

    //почистим память
    Bookshelf->root->delete_tree();
}
