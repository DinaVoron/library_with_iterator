#pragma once
#define Header
#include <iostream>

using namespace std;

struct card_struct
{
    int number; //����������� ����� �����
    int department_n; //����� ������
    string author; //�����
    string name; //��������
    bool refund; //������� ����� ��� ���
};


class Person
{
public:
    Person()
    {
        _philologist = (rand() % 2 == 0);
    }
    void Live();
    void Laugh();
    void Love();
    bool isPhilologist();
private:
    bool _philologist;
protected:
};


class Book
{
public:
    Book()
    {
    }
    explicit Book(int number, string author, string name, int department_n)
    {
        _number = number;
        _author = author;
        _name = name;
        _department_n = department_n;
        _state = true;
    }
    explicit Book(card_struct book_info)
    {
        _number = book_info.number;
        _author = book_info.author;
        _name = book_info.name;
        _department_n = book_info.department_n;
        _state = true;
    }
    ~Book() = default;
    void set_state(bool st);
    string get_author();
    string get_name();
    bool get_state();
    card_struct get_inf();
private:
    int _number;
    string _author;
    string _name;
    int _department_n; //����� ������, ��� �������� �����
    bool _state; //������� ����� (true) ��� ��� (false)
};


class eBook : public Book
{
public:
    eBook(int number, string author, string name, int department_n) :
        Book(number, author, name, department_n) {}
private:

protected:
};


class Card
{
public:
    Card()
    {
        _last = -1;
    }
    ~Card() = default;
    void set_last(int l);
    card_struct get_last_inf();
    void clean();
    void add(Book book);
    void set_state(bool st);
    friend bool state(Card& card);
    friend int last_num(Card& card);
    int d_n(int n);
private:
    card_struct _card[15]; //��������, ���� ����������� �����
    int _last; //����� ��������� ������
};


class Reader : public Person
{
public:
    Reader() = default;
    ~Reader() = default;
    void take_book(Book&& book);
    void take_book_forever(Book&& book);
    friend int last(Reader& reader);
    card_struct get_last_card(); //�������� ������� ������ �� ����� ��������
    void set_reader_state(bool state);
    void set_reader_last(int num);
    void new_card();
    int get_card_num(int n);
private:
    Card _card = Card();
};

bool Person::isPhilologist()
{
    return _philologist;
}

void Person::Live()
{
    cout << "Live" << endl;
}

void Person::Laugh()
{
    cout << "Laugh" << endl;
}

void Person::Love()
{
    cout << "Love" << endl;
}

string Book::get_author()
{
    return _author;
}


string Book::get_name()
{
    return _name;
}

card_struct Book::get_inf()
{
    card_struct cur;
    cur.author = _author;
    cur.name = _name;
    cur.number = _number;
    cur.department_n = _department_n;
    cur.refund = false;
    return(cur);
}

void Book::set_state(bool st)
{
    _state = st;
}

void Reader::set_reader_state(bool state)
{
    this->_card.set_state(state);
}

bool Book::get_state()
{
    return _state;
}

void Card::set_state(bool st)
{
    _card[_last].refund = st;
}

void Card::set_last(int l)
{
    _last = l;
}

void Reader::set_reader_last(int num)
{
    this->_card.set_last(num);
}

bool state(Card& card)
{
    if (card._last == -1)
    {
        return true;
    }
    return(card._card[card._last].refund);
}

int last_num(Card& card)
{
    return(card._last);
}


card_struct Card::get_last_inf()
{
    return(_card[_last]);
}


void Card::add(Book book)
{
    _card[_last] = book.get_inf();
}

int Card::d_n(int n)
{
    return (_card[n].number);
}


void Reader::take_book_forever(Book&& book)
{
    //������� �������� � ����� �� ��������. ��� ��������� ����� ��������� ������������
}

int Reader::get_card_num(int n)
{
    return (_card.d_n(n));
      //return (this->get_card_num(n));

}

void Card::clean()
{
    _last = -1;
}

void Reader::new_card()
{
    this->_card.clean();
}

int last(Reader& reader)
{
    return(last_num(reader._card));
}


card_struct Reader::get_last_card()
{
    return _card.get_last_inf();
}


void Reader::take_book(Book&& book)
{
    if (&book != nullptr)
    {
        if (book.get_state())
        {
            if (state(this->_card) || isPhilologist()) //��������, �� ������� �� �������
            {
                if (last_num(this->_card) == 14) //���� �������� �����������, �� ������� �����
                {
                    _card.clean();
                }
                _card.set_last(last_num(this->_card) + 1);
                _card.add(book); //��������� ����� ����� � ��������
                book.set_state(false);
                cout << "OK" << endl;;
            }
            else
            {
                cout << "Return other book first" << endl;
            }
        }
        else
        {
            cout << "The book is busy" << endl;
        }
    }
    else
    {
        cout << "The book is not found" << endl;
    }
}

template<class T>
concept Comparable = std::is_same<T, class card_struct>::value;


template <Comparable T, Comparable U>
bool compare(T x, U y)
{
    if (x.author != y.author)
    {
        return false;
    }
    if (x.department_n != y.department_n)
    {
        return false;
    }
    if (x.name != y.name)
    {
        return false;
    }
    if (x.number != y.number)
    {
        return false;
    }
    return true;
}

