#pragma once

class Event
{
public:
	string _book;
	string _speaker;
	string _date;
	Event() {};
	Event(string book, string speaker, string date)
	{
		_book = book;
		_speaker = speaker;
		_date = date;
	}
	string get_book()
	{
		return _book;
	}
	string get_speaker()
	{
		return _speaker;
	}
	string get_date()
	{
		return _date;
	}
	void set_book(string book)
	{
		_book = book;
	}
	void set_speaker(string speaker)
	{
		_speaker = speaker;
	}
	void set_date(string date)
	{
		_date = date;
	}
};

class EventBuilder {
private:
public:
	virtual void BuildEvent() { }
	virtual void BuildBook() { }
	virtual void BuildSpeaker() { }
	virtual void BuildDate() { }
	virtual Event* GetEvent() { return 0; }
	EventBuilder() {};
};

Event* CreateEvent(EventBuilder& builder) {
	builder.BuildEvent();
	builder.BuildBook();
	builder.BuildSpeaker();
	builder.BuildDate();
	return builder.GetEvent();
}

class ClassicEventBuilder : public EventBuilder
{
private:
	string _book;
	string _speaker;
	string _date;
public:
	void BuildEvent() {
	};
	void BuildBook() {
		this->_book = "Some Classic Book";
		cout << "Choosing some classic book" << endl;
	}
	void BuildSpeaker() {
		this->_speaker = "Bykov";
		cout << "Choosing speaker" << endl;
	}
	void BuildDate() {
		this->_date = "Monday 18:00";
		cout << "Choosing date" << endl;
	}
	Event *GetEvent() {
		Event *event = new Event(_book, _speaker, _date);
		return event;
	}
};

class PremierEventBuilder : public EventBuilder
{
private:
	string _book;
	string _speaker;
	string _date;
public:
	void BuildEvent() {
	};
	void BuildBook() {
		this->_book = "Some New Book";
		cout << "Choosing some new book" << endl;
	}
	void BuildSpeaker() {
		this->_speaker = "Peiper";
		cout << "Choosing speaker" << endl;
	}
	void BuildDate() {
		this->_date = "Friday 17:00";
		cout << "Choosing date" << endl;
	}
	Event* GetEvent() {
		Event* event = new Event(_book, _speaker, _date);
		return event;
	}
};

class LibraryDirector
{
private:
	EventBuilder* _builder;
public:
	LibraryDirector(EventBuilder &&builder)
	{
		if (&builder == nullptr)
		{
			cout << "BuilderError" << endl;
		}
		else
		{
			_builder = &builder;
		}
	}
	Event* MakeEvent()
	{
		_builder->BuildBook();
		_builder->BuildSpeaker();
		_builder->BuildDate();
		return _builder->GetEvent();
	}
};